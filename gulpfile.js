const gulp = require("gulp");
const eslint = require("gulp-eslint");
const del = require("del");
const babel = require("gulp-babel");
const sourcemaps = require("gulp-sourcemaps");
// const path = require("path");
const deploy = require("./deploy");



gulp.task("watch", () => {
  gulp.watch("src/**/*.*", ["default"]);
});

gulp.task("default", ["deploy"]);


gulp.task("compile", ["lint"], () => {
  return gulp.src("src/*.js")
    .pipe(sourcemaps.init())
    .pipe(babel({
      "presets": [[
        "@babel/env", {
          "targets": {
            "node": "8.9.3",
          },
        },
      ]],
      "plugins": ["autobind-class-methods", "transform-class-properties"],
    }))
    .pipe(sourcemaps.write(".", {includeContent: false, sourceRoot: "../src/"}))
    .pipe(gulp.dest("build/"));
});



gulp.task("deploy", ["compile"], async(callback) => {
  await deploy();
  // console.log("def?");
  // return callback();

});



gulp.task("lint", ["clean"], () => {
  return gulp.src(["src/**/*.js"])
    .pipe(eslint({
      fix: true,
    }))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});


gulp.task("clean", () => {
  return del(["build/**/*"]);
});
