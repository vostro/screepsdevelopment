
require("dotenv").config();
const {promisify} = require("util");
const readFile = promisify(require("fs").readFile);
const path = require("path");
const glob = promisify(require("glob"));
const https = require("https");
const email = process.env.SCREEPS_EMAIL;
const password = process.env.SCREEPS_PASSWORD;

module.exports = async() => {
  const files = await glob(`${path.resolve(__dirname, "./build")}/*.js`);
  const modules = await files.reduce((p, f) => {
    return p.then(async(o) => {
      const fileName = path.basename(f, ".js");
      o[fileName] = await readFile(f, {encoding: "utf8"});
      return o;
    });
  }, Promise.resolve({}));
  const data = {
    branch: process.env.SCREEPS_BRANCH,
    modules,
  };
  // console.log("data", data);
  var req = https.request({
    hostname: "screeps.com",
    port: 443,
    path: "/api/user/code",
    method: "POST",
    auth: email + ":" + password,
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    }
  });
  req.write(JSON.stringify(data));
  req.end();
};

