# Screeps Development

## Software

- SourceTree - https://www.sourcetreeapp.com/
- NodeJS 8.x - (https://nodejs.org/en/)
- Yarn - (https://yarnpkg.com/en/)
- Screeps
- VSCode - https://code.visualstudio.com/
  - Extensions
    - eslint
    - beautify
    - DotENV
    - QuickTask

## Setup

- Install Source Tree, NodeJS, Yarn, Screeps, VSCode + Extensions

- Use SourceTree to clone this repository

- Checkout your assigned branch

- Open the newly created directory in VSCode

- Execute the following in a terminal/command prompt in the same directory as the package.json file. Press `Ctrl + ~` in VSCode to open this. This will install all the dependencies for the project as well as gulp as a standard command in your terminal

```bash
yarn global add gulp
yarn
```

- create a file called `.env` in the projects directory and fill it with the following, replacing the email, password with the one from your assigned screeps account. Please contact matthew if you do not have this information.

```dotenv
SCREEPS_EMAIL="email@address.com"
SCREEPS_PASSWORD=1111
SCREEPS_BRANCH=test
```

## Running Project

- Open Folder in VSCode

- Menu -> Tasks -> Run Build Task (gulp watch) or Ctrl + Shift + B

or 

- Open Folder in Terminal/Command Prompt

- Execute `gulp watch`

If the project has started correctly you should see the following

```bash
> Executing task: node_modules/.bin/gulp watch <

[12:53:02] Using gulpfile ~/projects/screeps/gulpfile.js
[12:53:02] Starting 'watch'...
[12:53:02] Finished 'watch' after 12 ms
```

This will now watch the `src/*.js` directory and it will validate and compile the files and upload them to the screeps server on the branch you supplied in the `.env` file

The following is an example of a successful compile and upload.

```bash
> Executing task: node_modules/.bin/gulp watch <

[12:53:02] Using gulpfile ~/projects/screeps/gulpfile.js
[12:53:02] Starting 'watch'...
[12:53:02] Finished 'watch' after 12 ms
[12:56:53] Starting 'clean'...
[12:56:53] Finished 'clean' after 14 ms
[12:56:53] Starting 'lint'...
[12:56:54] Finished 'lint' after 538 ms
[12:56:54] Starting 'compile'...
[12:56:54] Finished 'compile' after 331 ms
[12:56:54] Starting 'deploy'...
[12:56:54] Finished 'deploy' after 27 ms
[12:56:54] Starting 'default'...
[12:56:54] Finished 'default' after 59 μs
```

## Reference Material

- http://docs.screeps.com

- https://www.codecademy.com/learn/introduction-to-javascript

- http://dmitrysoshnikov.com/ecmascript/javascript-the-core-2nd-edition/

- http://es6katas.org/

- https://babeljs.io/learn-es2015/

## Ideas to start with

- Follow the tutorials in http://docs.screeps.com
- Try to replicate a RTS (e.g. Starcraft), taking note that the code needs to be giving the commands and also directly controlling the units.
- Search GitHub for open source screeps projects for examples and ideas.
